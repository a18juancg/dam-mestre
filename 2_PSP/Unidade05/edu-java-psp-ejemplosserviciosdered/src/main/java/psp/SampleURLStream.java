package psp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class SampleURLStream {
    public static void main(final String[] args) throws IOException {
        final URL url = new URL("http://ftp.rediris.es/debian/README.mirrors.txt");

        // conecta a esa URL
        url.openConnection();

        // asocia un flujo de entrada a la conexión URL
        final InputStream flujoIn = url.openStream();

        // crea un flujo de salida asociado a destino
        final FileOutputStream flujoOutFile = new FileOutputStream("target/FicheroSampleURLStream.txt");

        final byte[] buffer = new byte[20];
        int bytesLeidos, totalBytesLeidos = 0;
        // mientras hay bytes
        while((bytesLeidos = flujoIn.read(buffer)) > 0) {
            // almacena lo que lee en el buffer
            flujoOutFile.write(buffer, 0, bytesLeidos);
            totalBytesLeidos += bytesLeidos;
        }
        System.out.println(String.format("Total bytes leídos %d", totalBytesLeidos));
        flujoOutFile.close();
     }
}