# Requisitos: seguridad, vivacidad, eficiencia y reusabilidad

Como cualquier aplicación, los programas concurrentes deben cumplir una serie de requisitos de calidad. En este apartado, veremos **algunos aspectos** que nos permitirán desarrollar proyectos concurrentes con, casi, la completa certeza de que estamos desarrollando software **de calidad**.

Todo programa concurrente debe satisfacer dos tipos de propiedades:

* Propiedades de **seguridad** ("safety"): estas propiedades son relativas a que en cada instante de la ejecución no debe haberse producido algo que haga entrar al programa en un estado erróneo:

	* Dos procesos **no deben entrar simultáneamente en una sección crítica**.

	* **Se respetan las condiciones de sincronismo**, como: el consumidor no debe consumir el dato antes de que el productor los haya producido; y, el productor no debe producir un dato mientras que el buffer de comunicación esté lleno.

* Propiedades de **vivacidad** ("liveness"): cada sentencia que se ejecute conduce en algún modo a un avance constructivo para alcanzar el objetivo funcional del programa. Son, en general, muy dependientes de la política de planificación que se utilice. Ejemplos de propiedades de vivacidad son:

	* **No** deben producirse **bloqueos activos (livelock)**. Conjuntos de procesos que ejecutan de forma continuada sentencias que no conducen a un progreso constructivo.

	* **Aplazamiento indefinido (starvation)**: consiste en el estado al que puede llegar un programa que aunque potencialmente puede avanzar de forma constructiva, no lo hace. Esto puede suceder, como consecuencia de que no se le asigna tiempo de procesador en la política de planificación o porque en las condiciones de sincronización hemos establecido criterios de prioridad que perjudican siempre al mismo proceso.

	* **Interbloqueo (deadlock)**: se produce cuando los procesos no pueden obtener, nunca, los recursos necesarios para finalizar su tarea. Vimos un ejemplo de esta situación en el apartado 4.2 Condiciones de competencia.


Es evidentemente, que también nos preocuparemos por diseñar nuestras aplicaciones para que sean
**eficientes**:

* **No** utilizarán **más recursos de los necesarios**.

* Buscaremos la rigurosidad en su implementación: **toda la funcionalidad esperada de forma correcta y concreta**.

Y, en cuanto a la **reusabilidad**, debemos tenerlo ya, muy bien aprendido:

* Implementar el código de **forma modular**: definiendo clases, métodos, funciones, ...
* **Documentar** correctamente el código y el proyecto.

**Para conseguir todos lo anterior contaremos con los patrones de diseño y pondremos especial cuidado en
documentar y depurar convenientemente**.

## Arquitecturas y patrones de diseño

**La Arquitectura del Software, también denominada arquitectura lógica, es el diseño de más alto nivel de la estructura de un sistema. Consiste en un conjunto de patrones y abstracciones coherentes con base a las cuales se pueden resolver los problemas**. A semejanza de los planos de un edificio o construcción, estas indican la estructura, funcionamiento e interacción entre las partes del software.

La arquitectura de software, tiene que ver con el diseño y la implementación de estructuras de software de alto nivel. Es el resultado de ensamblar un cierto número de elementos arquitectónicos de forma adecuada para **satisfacer** la **mayor funcionalidad** y **requerimientos** de desempeño de un sistema, así como **requerimientos no funcionales**, como la confiabilidad, escalabilidad, portabilidad, y disponibilidad; mantenibilidad, auditabilidad, flexibilidad e interacción.

Generalmente, no es necesario inventar una nueva arquitectura de software para cada sistema de información. Lo habitual es adoptar una arquitectura conocida en función de sus ventajas e inconvenientes para cada caso en concreto. Así, las arquitecturas más universales son:

* **Monolítica**. El software se estructura en grupos funcionales muy acoplados.
* **Cliente-servidor**. El software reparte su carga de cómputo en dos partes independientes: consumir un servicio y proporcionar un servicio.
* **Arquitectura de tres niveles**. Especialización de la arquitectura cliente-servidor donde la carga se divide en capas con un reparto claro de funciones: una capa para la presentación (interfaz de usuario), otra para el cálculo (donde se encuentra modelado el negocio) y otra para el almacenamiento (persistencia). Una capa solamente tiene relación con la siguiente.


**Otras arquitecturas menos conocidas son**:

* En pipeline. Consiste en modelar un proceso comprendido por varias fases secuenciales, siendo la entrada de cada una la salida de la anterior.

* Entre pares. Similar a la arquitectura cliente-servidor, salvo porque podemos decir que cada elemento es igual a otro (actúa simultáneamente como cliente y como servidor).

* En pizarra. Consta de múltiples elementos funcionales (llamados agentes) y un instrumento de control o pizarra. Los agentes estarán especializados en una tarea concreta o elemental. El comportamiento básico de cualquier agente, es: examinar la pizarra, realizar su tarea y escribir sus conclusiones en la misma pizarra. De esta manera, cualquier agente puede trabajar sobre los resultados generados por otro. 

* Orientada a servicios (Service Oriented Architecture - SOA) Se diseñan servicios de aplicación basados en una definición formal independiente de la plataforma subyacente y del lenguaje de programación, con una interfaz estándar; así, un servicio C# podrá ser usado por una aplicación Java. 

* Dirigida por eventos. Centrada en la producción, detección, consumo de, y reacción a eventos.

**Los patrones de diseño, se definen como soluciones de diseño que son válidas en distintos contextos y que han sido aplicadas con éxito en otras ocasiones:**

* Ayudan a "arrancar" en el diseño de un programa complejo.
	* Dan una descomposición de objetos inicial "bien pensada". 
	* Pensados para que el programa sea escalable y fácil de mantener. 
	* Otra gente los ha usado y les ha ido bien.
* Ayudan a reutilizar técnicas.
	* Mucha gente los conoce y ya sabe como aplicarlos. 
	* Están en un alto nivel de abstracción.
	* El diseño se puede aplicar a diferentes situaciones.

Existen **dos modelos básicos de programas concurrentes:**

* Un programa resulta de la actividad de objetos activos que interaccionan entre si directamente **o a través de recursos y servicios pasivos**.

* Un programa resulta de la ejecución concurrente de tareas. Cada tarea es una **unidad de trabajo abstracta y discreta que idealmente puede realizarse con independencia de las otras tareas.**

**No es obligatorio** utilizar patrones, solo es aconsejable en el caso de tener el mismo problema o similar que soluciona el patrón, siempre teniendo en cuenta que en un caso particular puede no ser aplicable.

Como podemos ver en [SourceMaking](https://sourcemaking.com/design_patterns), lo normal es que encontremos la definición de los patrones de diseño en UML (Unified Modeling Language – Lenguaje Unificado de Modelado), con ejemplos de patrones de diseño implementados.

Aunque un diagrama UML es muy fácil de entender, en [DocIRS](https://www.docirs.cl/uml.htm), encontrarás un tutorial sobre UML.


### Para saber más

En 1990 se publicó un libro que recopilaba 23 patrones de diseño comunes. [Este libro](http://wiki.c2.com/?DesignPatternsBook) marcó el punto de inicio en el que los patrones de diseño tuvieron gran éxito en el mundo de la informática. En la actualidad, seguimos haciendo uso de esos 23 patrones y algunos adicionales, sobre todo relacionados con el diseño de interfaces web.

La arquitectura [SOA](http://docs.oasis-open.org/soa-rm/v1.0/soa-rm.pdf) es de gran importancia en el desarrollo de aplicaciones en el mundo empresarial, porque diseñar siguiendo esta arquitectura, permite una gran integración e interoperabilidad entre servicios y aplicaciones.


## Documentación

Para la documentación de nuestras aplicaciones tendremos en cuenta:

> **Hay que añadir explicaciones a todo lo que no es evidente. No hay que repetir lo que se hace, sino explicar por qué se hace**.

Documentando nuestro código responderemos a estas preguntas:

* ¿De qué se encarga una clase? ¿Un paquete?
* ¿Qué hace un método? ¿Cuál es el uso esperado de un método? 
* ¿Para qué se usa una variable? ¿Cuál es el uso esperado de una variable?
* ¿Qué algoritmo estamos usando? ¿De dónde lo hemos sacado? 
* ¿Qué limitaciones tiene el algoritmo? ¿... la implementación?
* ¿Qué se debería mejorar ... si hubiera tiempo?

En la siguiente tabla tenemos un resumen de los distintos tipos de comentarios en Java:

|    | Javadoc | Línea | Tipo C
| --- | ------- | ----- | ------
| Sintaxis | Comienzan con `/**`, se pueden prolongar a lo largo de varias líneas (que probablemente comiencen con el carácter `*`) y terminan con los caracteres `*/`. Cuenta con etiquetas específicas tipo: `@author`, `@param`, `@return`,... | Comienzan con `//` y terminan con la línea. | Comienzan con `/*`, se pueden prolongar a lo largo de varias líneas (que probablemente comiencen con el carácter `*`) y terminan con los caracteres `*/`.
| Propósito | Generar documentación externa | Documentar código que no necesitamos que aparezca en la documentación externa. | Eliminar código.
| Uso | Al principio de cada clase. Al principio de cada método. Antes de cada variable de clase. | Al principio de un fragmento de código no evidente. A lo largo de los bucles. Siempre que hagamos algo raro o que el código no sea evidente. | Ocurre a menudo que código obsoleto no queremos que desaparezca, sino mantenerlo "por si acaso". Para que no se ejecute, se comenta.

Además de lo anterior, al documentar nuestras aplicaciones concurrentes destacaremos:

* **Las condiciones de sincronismo** que se hayan implementado en la clase o método (en la documentación javadoc).

* **Destacaremos las regiones críticas** que hayamos identificado y el recurso que compartido a proteger.
 

## Dificultades en la depuración

Cuando estamos programando aplicaciones que incluyen mecanismos de sincronización y acceden a recursos de forma concurrente junto con otras aplicaciones, a la hora de depurarlas, nos enfrentaremos a:

* Los mismos problemas de depuración de una aplicación secuencial.

* Además de nuevos errores de temporización y sincronización propios de la programación concurrente.

Los programas secuenciales presentan una línea simple de control de flujo. Las operaciones de este tipo de programas están estrictamente ordenados como una secuencia temporal lineal.

* El comportamiento del programa es solo función de la naturaleza de las operaciones individuales que constituye el programa y del orden en que se ejecutan.

* En los programas secuenciales, el tiempo que tarda cada operación en ejecutarse no tiene consecuencias sobre el resultado.

* Para validar un programa secuencial necesitaremos comprobar: 
	* La correcta respuesta a cada sentencia.
	* El correcto orden de ejecución de las sentencias.

**Para validar un programa concurrente se requiere** comprobar los mismos aspectos que en los programas secuenciales, además de **los siguientes nuevos aspectos**:

* Las sentencias se pueden **validar individualmente** solo si no están involucradas en el uso de recursos compartidos.

* Cuando existen recursos compartidos, los efectos de interferencia entre las sentencias concurrentes pueden ser muy variados y la validación es muy difícil. Comprobaremos la **corrección en la definición de las regiones críticas** y que **se cumple la exclusión mutua**.

* Al comprobar la correcta implementación del sincronismo entre aplicaciones (que es forzar la ejecución secuencial de tareas de distintos procesos introduciendo sentencias explícitas de sincronización) tendremos en cuenta que **el tiempo no influye sobre el resultado**.

El problema es que las herramientas de depuración no nos proporcionan toda la funcionalidad que quisiéramos para poder depurar nuestros programas concurrentes.

¿Con qué herramientas contamos para depurar programas concurrentes?

* **El depurador** del IDE NetBeans. En la Unidad 2, veremos que el depurador de NetBeans, sí está preparado para la depuración concurrente de hilos dentro de un mismo proceso. En esta unidad estamos tratando procesos independientes.

* **Hacer volcados de actividad en un fichero de log** o en pantalla de salida (nos permitirá hacernos una idea de lo que ha estado pasando durante las pruebas de depuración).

* **Herramientas de depuración específicas**: `TotalView` (SW comercial), `StreamIt Debugger Tool` (plugin para eclipse), ...


**Una de las nuevas situaciones a las que nos enfrentamos es que a veces, los errores que parecen estar sucediendo, pueden desaparecer cuando introducimos código para tratar de identificar el problema.**

Nos damos cuenta de la complejidad que entraña depurar el comportamiento de aplicaciones concurrentes, es por ello, que al diseñarlas, tendremos en cuenta los **patrones de diseño**, que ya están diseñados resolviendo errores comunes de la concurrencia. Podemos verlos como 'recetas', que nos permiten resolver los problemas 'tipo' que se presentan en determinadas condiciones de sincronismo y/o en los accesos concurrentes a un recurso.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)
