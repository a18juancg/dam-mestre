# Bibliotecas de clases y componentes Java

Java se ha construido con extensas capacidades de interconexión TCP/IP y soporta diferentes niveles de conectividad en red, facilitando la creación de aplicaciones cliente/servidor y generación de servicios en red. Así por ejemplo: permite abrir una URL como por ejemplo http://www.dominio.es/public/archivo.pdf, permite realizar una invocación de métodos remotos, RMI, o permite trabajar con sockets.

**El paquete principal que proporciona el API de Java para programar aplicaciones con comunicaciones en red es:**

* `java.net`. Este paquete soporta clases para generar diferentes servicios de red, servidores y clientes.

**Otros paquetes Java para comunicaciones y servicios en red son:**

* `java.rmi`. Paquete que permite implementar una interface de control remoto (RMI).

* `javax.mail`. Permite implementar sistemas de correo electrónico.

Para ciertos servicios estándar, Java no proporciona objetos predefinidos y por tanto una solución fácil para generarlos es recurrir a bibliotecas externas, como por ejemplo, el API proporcionado por *Apache Commons Net* (las bibliotecas `org.apache.commons.net`). Esta API permite implementar aplicaciones cliente para los protocolos estándar más populares, como: Telnet, FTP, o FTP sobre HTTP entre otros. En el siguiente [recurso adicional](http://commons.apache.org/proper/commons-net/) puedes consultar todas las funcionalidades que ofrece el *API Apache Commons Net*.

## Objetos predefinidos

El paquete `java.net, proporciona una API que se puede dividir en dos niveles:

* **Una API de bajo nivel**, que permite representar los siguientes objetos:
	* **Direcciones**. Son los identificadores de red, esto es, las direcciones IP.
		* Clase `InetAddress`. Implementa una dirección IP. 
	* **Sockets**. Son los mecanismos básicos de comunicación bidireccional de datos.
		* Clase `Socket`. Implementa un extremo de una conexión bidireccional.
		* Clase `ServerSocket`. Implementa un socket que los servidores pueden utilizar para escuchar y aceptar peticiones de clientes.
		* Clase `DatagramSocket`. Implementa un socket para el
envío y recepción de datagramas.
		* Clase `MulticastSocket`. Representa un socket datagrama, útil para enviar paquetes multidifusión.
	* **Interfaces**. Describen las interfaces de red.
		* Clase `NetworkInterface`. Representa una interfaz de red compuesta por un nombre y una lista de direcciones IP asignadas a esta interfaz.

* **Una API de alto nivel**, que se ocupa de representar los siguientes objetos, mediante las clases que te indicamos:
	* **URI**. Representan los identificadores de recursos universales.
		* Clase `URI`.
	* **URL**. Representan localizadores de recursos universales. 
		* Clase `URL`. Representa una dirección URL.
	* **Conexiones**. Representa las conexiones con el recurso apuntado por URL.
		* Clase `URLConnection`. Es la superclase de todas las clases que representan un enlace de comunicaciones entre la aplicación y una URL.
		* Clase `HttpURLConnection`. Representa una URLConnection con soporte para HTTP y con ciertas caracteríaticas especiales.

**Mediante las clases de alto nivel se consigue una mayor abstracción, de manera que, esto facilita bastante la creación de programas que acceden a los recursos de la red**. A continuación veremos algunos ejemplos.

![Ilustración Pila Api Java Net](img/api-java-net.png)

## Métodos y ejemplos de uso de `InetAddress`

La clase `InetAddress` proporciona objetos que puedes utilizar para manipular tanto direcciones IP como nombres de dominio. También proporciona métodos para resolver los nombres de host a sus direcciones IP y viceversa.

Una instancia de InetAddress consta de una dirección IP y en algunos casos también del nombre de host asociado. Esto último depende de si se ha creado con el nombre de host o bien ya se ha aplicado la resolución de nombres.

Esta clase **no tiene constructores**. Sin embargo, **`InetAddress` dispone de métodos estáticos que devuelven objetos `InetAddress`**. Te indicamos cuáles son esos métodos:

* `getLocalHost()`. Devuelve un objeto de tipo InetAddress con los datos de direccionamiento de mi equipo en la red local (no del conocido localhost).

* `getByName(String host)`. Devuelve un objeto de tipo InetAddress con los datos de direccionamiento del host que le pasamos como parámetro. Donde el parámetro host tiene que ser un nombre o IP válido, ya sea de Internet (como iesalandalus.org o 195.78.228.161), o de tu red de área local (como documentos.servidor o 192.168.0.5). Incluso puedes poner localhost, o cualquier otra IP o nombre NetBIOS de tu red local.

* `getAllByName(String host)`. Devuelve un array de objetos de tipo InetAddress con los datos de direccionamiento del host pasado como parámetro. Recuerda que en Internet es frecuente que un mismo dominio tenga a su disposición más de una IP.

Todos estos métodos pueden generar una excepción `UnknownHostException` si no pueden resolver el nombre pasado como parámetro.

Algunos **métodos interesantes de un objeto `InetAddress`** para resolver nombres son los siguientes: 

* `getHostAddress()`. Devuelve en una cadena de texto la correspondiente IP.

* `getAddress()`. Devuelve un array formado por los grupos de bytes de la IP correspondiente. 

Otros métodos interesantes de esta clase son:

* `getHostName()`. Devuelve en una cadena de texto el nombre del host.

* `isReachable(int tiempo)`. Devuelve `TRUE` o `FALSE` dependiendo de si la dirección es alcanzable en el tiempo indicado en el parámetro.

En el siguiente [recurso adicional]() tienes un ejemplo de uso de `InetAddress`. Con este ejemplo tratamos de ilustrar el uso de los métodos anteriores para resolver algunos nombres a su dirección o direcciones IP, tanto en la red local como en Internet. Por ello, para probarlo, debes tener conexión a Internet, en otro caso sólo se ejecutará la parte relativa a la red local, y se lanzará la correspondiente excepción al intentar resolver nombres de Internet.

## Programación con URL

La programación de URL se produce a un nivel más alto que la programación de sockets y ésto, puede facilitar la creación de aplicaciones que acceden a recursos de la red.

¿Tienes claro lo que es una URL y las partes en las que se puede descomponer? Vamos a verlo.

Una **URL**, *Localizador Uniforme de Recursos*, representa una dirección a **un recurso de la World Wide Web**. Un recurso puede ser algo tan simple como un archivo o un directorio, o puede ser una referencia a un objeto más complicado, como una consulta a una base de datos, el resultado de la ejecución de un programa, etc.

![Ilustración Formato URL](img/formatoURL.png)

Consideraciones sobre una URL:

* Puede referirse a sitios web, ficheros, sitios ftp, direcciones de correo electrónico, etc.
* La **estructura de una URL** se puede dividir en varias partes, tal y como puede ver en la imagen previa.
* *Protocolo*. El protocolo que se usa para comunicar.
* *Nombrehost*. Nombre del host que proprociona el servicio o servidor.
* *Puerto*. El puerto de red en el servidor para conectarse. Si no se especifica, se utiliza el puerto por defecto para el protocolo.
* *Ruta*. Es la ruta o path al recurso en el servidor.
* *Referencia*. Es un fragmento que indica una parte específica dentro del recurso especificado.

Por ejemplo, algunas posibles direcciones URL serían las siguientes:

* `http://www.iesalandalus.org/organizacion.htm`, el protocolo utilizado es el `http`, el nombre del host `www.iesalandalus.org` y la ruta es `organización.htm`, que en este caso es una página html. Al no indicar puerto, se toma el puerto por defecto para HTTP que es el `80`. 

* `http://www.iesalandalus.org:85/organizacion.htm`, en este caso se está indicando como puerto el `85`. `http://www.dominio.es/public/pag.hml#apartado1`, en este caso se especifica como ruta `/public/pag.html#apartado`, por lo que una vez recuperado el archivo `pag.htm` se está indicando mediante la referencia `#apartado1` que interesa el `aparatdo1` dentro de esa página.

En una URL se pueden especificar otros protocolos además de HTTP. Consulta el siguiente [recurso adicional](https://es.wikipedia.org/wiki/Localizador_de_recursos_uniforme#Esquema_URL) para ver esos protocolos y más ejemplos de posibles URL.

> **La clase URL de Java permite representar un URL**

Utilizando la clase URL, se puede establecer una conexión con cualquier recurso que se encuentre en Internet, o en nuestra Intranet. Una vez establecida la conexión, invocando los métodos apropiados sobre ese objeto URL se puede obtener el contenido del recurso en el cliente. Esto es una idea potentísima, pues permite, en teoría, descargar el contenido de cualquier recurso en el cliente, incluso aunque se requiriera un protocolo que no existía cuando se escribió el programa.

Pero la clase URL también proporciona una forma alternativa de conectar un ordenador con otro y compartir datos, basándose en *streams*. Esto último, es algo que también se puede hacer con la programación de sockets, tal y como has visto en la unidades anteriores.

## Crear y analizar objetos URL

¿Cómo podemos crear objetos URL? La clase URL dispone de diversos constructores para crear objetos tipo URL que se diferencian en la forma de pasarle la dirección URL. Por ejemplo, se puede crear un objeto URL:

1. A partir de todos o algunos de los elementos que forman
parte de una URL, como por ejemplo:
`URL url=new URL("http", "www.iesalandalus.org",80","index.htm")`.
Crea un objeto URL a partir de los componentes indicados (protocolo, host, puerto, fichero), esto es, crea la URL: `http://www.iesalandalus.org:80/index.htm`.

2. A partir de la cadena especificada, dejando que el sistema utilice todos los valores por defecto que tiene definidos, como por ejemplo:
`URL url=new URL("http://www.iesalandalus.org")`
que crearía la URL: `http://www.iesalandalus.org`.

3. A partir de una ruta relativa pasada como primer parámetro.

4. A partir de las especificaciones indicadas y un manejador del protocolo que conoce cómo comunicarse con el servidor.

Cada uno de los constructores de URL puede lanzar una `MalformedURLException` si los argumentos del constructor son nulos o el protocolo es desconocido. Lo normal, es capturar y manejar esta excepción mediante un bloque `try-catch`.

> **Las URLs son objetos de una sola escritura**. Lo que significa, que una vez que has creado un objeto URL no se puede cambiar ninguno de sus atributos (protocolo, nombre del host, nombre del fichero ni número de puerto).

**Se puede analizar y descomponer una URL** utilizando los siguientes métodos:

* `getProtocol()`. Obtiene el protocolo de la URL.
* `getHost()`. Obtiene el host de la URL.
* `getPort()`. Obtiene el puerto de la URL, si no se ha especificado obtiene -1. 
* `getDefaultPort()`. Obtiene el puerto por defecto asociado a la URL, si no lo tiene obtiene -1. 
* `getFile()`. Obtiene el fichero de la URL o una cadena vacía si no existe.
* `getRef()`. Obtiene la referencia de la URL o null si no la tiene.

## Leer y escribir a través de una `URLConnection`

Un objeto `URLConnection` se puede utilizar para leer desde y escribir hacia el recurso al que hace referencia el objeto URL.

De entre los muchos métodos que nos permiten trabajar con conexiones URL vamos a centrarnos en primer lugar en los siguientes:

* `URL.openConnection()`. Devuelve un objeto URLConnection que
representa una nueva conexión con el recurso remoto al que se
refiere la URL.

* `URL.openStream()`. Abre una conexión a esta dirección URL y
devuelve un InputStream para la lectura de esa conexión. Es una abreviatura de: `openConnection().getInputStream()`.

Te mostramos a continuación **un ejemplo** muy sencillo que lee una URL, basándose tan solo en estos dos métodos. Los pasos a seguir para leer la URL son:

1. Crear el objeto URL mediante `URL url=new URL(...);`
2. Obtener una conexión con el recurso especificado mediante `URL. openConnection()`.
3. Abrir conexión con esa URL mediante `URL.openStream()`.
4. Manejar los flujos necesarios para realizar la lectura

Por ejemplo, podemos utilizar objetos URL para leer un archivo de texto y almacenarlo en un fichero local, tal y como puedes ver en el siguiente segmento de código:

```java
URL url = new URL("http://ftp.rediris.es/debian/README.mirrors.txt");

// conecta a esa URL
url.openConnection();
    
// asocia un flujo de entrada a la conexión URL
InputStream flujoIn = url.openStream();
    
// crea un flujo de salida asociado a destino
FileOutputStream flujoOutFile = new FileOutputStream("target/FicheroSampleURLStream.txt");
    
byte[] buffer = new byte[20];
int bytesLeidos, totalBytesLeidos = 0;

// mientras hay bytes
while((bytesLeidos = flujoIn.read(buffer)) > 0) {
    // almacena lo que lee en el buffer
    flujoOutFile.write(buffer, 0, bytesLeidos);
    totalBytesLeidos += bytesLeidos;
}
System.out.println(String.format("Total bytes leídos %d", totalBytesLeidos));
flujoOutFile.close();
```

También podemos utilizar objetos URL para leer una URL y analizar su código fuente, tal y como hacen los buscadores, optimizadores de código o validadores de código.

## Trabajar con el contenido de una URL

El método que permite **obtener el contenido de un objeto URL** es:

* `URL.getContent()`. Devuelve el conenido de esa URL. Este método determina en primer lugar el tipo de contenido del objeto llamando al método getContentType(). Si esa es la primera vez que la aplicación ha visto ese tipo de contenido específico, se crea un manejador para dicho tipo de contenido.

Mediante este método y otros proporcionados por la clase **`URLConnection`**, veremos al final de este apartado, un ejemplo de cómo examinar el contenido del objeto URL y realizar la tarea que interese.

**Crear una conexión URL** realmente implica los siguientes pasos:

1. Crear un objeto URLConnection, éste se crea cuando se llama al método openConnection() de un objeto URL.
2. Establecer los parámetros y propiedades de la petición.
3. Establecer la conexión utilizando el método connect().
4. Se puede obtener información sobre la cabecera o/y obtener el recurso remoto.

A partir de un objeto `URLConnection`, se puede obtener información muy útil sobre la conexión que se haya establecido y existen muchos métodos en la clase URLConnection que se pueden utilizar para examinar y manipular el objeto que se crea de este tipo.

![Ilustración URL Connection](img/URLconnection.png)

Vamos a ver algunos métodos que serán de utilidad en nuestro ejemplo:

* **`connect()`**. Establece una conexión entre la aplicación (el cliente) y el recurso (el servidor), permite interactuar con el recurso y consultar los tipos de cabeceras y contenidos para determinar el tipo de recurso de que se trata.

* **`getContentType()`**. Devuelve el valor del campo de cabecera `content-type` o `null` si no está definido. 

* **`getContentLength()`**. Devuelve el valor del campo de cabecera `content-length` o `-1` si no está definido. 

* **`getLastModified()`**. Devuelve la fecha de la última modificación del recurso.

A continuación dispones de una presentación que muestra la creación de una aplicación para acceder a recursos de Internet a través de objetos URL, esto es, un cliente web.

![Captura portada presentación Flash](doc/PSP05_CONT_R23_AccesoRecursosURL/captura.png)
[SWF](doc/PSP05_CONT_R23_AccesoRecursosURL/flash.swf) [HTML](doc/PSP05_CONT_R23_AccesoRecursosURL/flash.html) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-ejemplosserviciosdered/src/master/src/main/java/psp/sampleaccesorecursosurl/)

Se puede utilizar la clase `URL` para escribir manejadores de protocolos o cosas parecidas. Para ello, será necesario conocer en profundidad la clase `URLConnection`. Consulta este [recurso adicional](https://docs.oracle.com/javase/7/docs/api/java/net/URLConnection.html) para ello.

En este [recurso adicional](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields) puedes consultar todos los parámetros de las cabeceras del protocolo HTTP.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)