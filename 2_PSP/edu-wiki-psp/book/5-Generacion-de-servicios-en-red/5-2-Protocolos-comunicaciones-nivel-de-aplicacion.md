# Protocolos de comunicaciones del nivel de aplicación

En la actualidad, el conjunto de protocolos TCP/IP constituyen el modelo más importante sobre el que se basa la comunicación de aplicaciones en red. Esto es debido, no sólo al espectacular desarrollo que ha tenido Internet, en los últimos años (recuerda que TCP/IP es el protocolo que utiliza Internet), sino porque también, TCP/IP ha ido cobrando cada vez más protagonismo en las redes locales y   corporativas.

Dentro de la jerarquía de protocolos TCP/IP **la capa de Aplicación** ocupa el nivel superior y es precisamente la que **incluye los protocolos de alto nivel relacionados con los servicios en red** que te hemos indicado antes.

**La capa de Aplicación define los protocolos (normas y reglas) que utilizan las aplicaciones para intercambiar datos**. En realidad, hay tantos protocolos de nivel de aplicación como aplicaciones distintas; y además, continuamente se desarrollan nuevas aplicaciones, por lo que el número de protocolos y servicios sigue creciendo.

En la siguiente imagen puedes ver un esquema de la familia de protocolos TCP/IP y su organización en capas o niveles.

![Ilustración pila de protocolos TCP/IP](img/pilaProtocolosTCP-IP.png)

A continuación, vamos a destacar, por su importancia y gran uso, **algunos de los protocolos estándar del nivel de aplicación**:

* **FTP**. Protocolo para la transferencia de ficheros.

* **Telnet**. Protocolo que permite acceder a máquinas remotas a través de una red. Permite manejar por completo la computadora remota mediante un intérprete de comandos.

* **SMTP**. Protocolo que permite transferir correo electrónico. Recurre al protocolo de oficina postal POP para almacenar mensajes en los servidores, en sus versiones POP2 (dependiente de SMTP para el envío de mensajes) y POP3 (independiente de SMTP).

* **HTTP**. Protocolo de transferencia de hipertexto.

* **SSH**. Protocolo que permite gestionar remotamente a otra compuatora de la red de forma segura. 

* **NNTP**. Protocolo de Transferencia de Noticias (en inglés *Network News Transfer Protocol*).

* **IRC**. Chat Basado en Internet (en inglés Internet Relay Chat)

* **DNS**. Protocolo para traducir direcciones de red.

## Comunicación entre aplicaciones

TCP/IP funciona sobre el concepto del modelo cliente/servidor, donde, como recordarás de unidades anteriores:

* **El Cliente** es el programa que ejecuta el usuario y solicita un servicio al servidor. Es quien inicia la comunicación.

* **El Servidor** es el programa que se ejecuta en una máquina (o varias) de red y ofrece un servicio (FTP, web, SMTP, etc.) a uno o múltiples clientes. Este proceso permanece a la espera de peticiones, las acepta y como respuesta, proporciona el servicio solicitado.

**La capa de aplicación es el nivel que utilizan los programas para comunicarse a través de una red con otros programas**. Los procesos que aparecen en esta capa, son aplicaciones específicas que pasan los datos al nivel de aplicación en el formato que internamente use el programa, y es codificado de acuerdo con un protocolo estándar. Una vez que los datos de la aplicación han sido codificados, en un protocolo estándar del nivel de aplicación, se pasan hacia abajo al siguiente nivel de la pila de protocolos TCP/IP.

En el nivel de transporte, las aplicaciones normalmente hacen uso de TCP y UDP, y son habitualmente asociados a un número de   puerto. Por ejemplo, el servicio web mediante HTTP utiliza por defecto el puerto 80, el servicio FTP el puerto 21, etc.

Por ejemplo, la **World Wide Web o Web, que usa el protocolo HTTP, sigue fielmente el modelo cliente/servidor**:

* Los clientes son las aplicaciones que permiten consultar las páginas web (navegadores) y los servidores, las aplicaciones que suministran (sirven) páginas web.

* Cuando un usuario introduce una dirección web en el navegador (una URL), por ejemplo `http://www.dominio.es`, éste solicita mediante el protocolo HTTP la página web al servidor web que se ejecuta en la máquina donde está la página.

* El servidor web envía la página por Internet al cliente (navegador) que la solicitó, y éste último la muestra al usuario.

![Ilustración Diálogo entre cliente y servidor HTTP](img/comunicacionHTTP.png)

**Los dos extremos dialogan siguiendo un protocolo de aplicación**, tal y como puedes ver en la imagen que representa el ejemplo anterior.

* El cliente solicita el recurso web `www.dominio.es` mediante `GET HTTP/1.0`
* El servidor le contesta `HTTP/1.0 200 OK` (todo correcto) y le envía la página html solicitada.

Más adelante veremos varios ejemplos de cómo implementar un servicio web básico.

En el siguiente [recurso adicional](https://es.wikipedia.org/wiki/Anexo:Números_de_puertos_de_red) dispones de un relación detallada de números de puerto y servicios que por defecto se le asocian, así como el protocolo de nivel de transporte que utilizan.

Los RFC (*Request For Comments*) documentan los protocolos de aplicación estándar. En el siguiente [recurso adicional](https://www.rfc-editor.org) puedes consultar los RFC.

## Conexión, transmisión y desconexión

**Los protocolos de aplicación se comunican con el nivel de transporte mediante un API, denominada API Socket**, y que en en el caso de Java viene implementada mediante las clases del paquete `java.net` como `Socket` y `ServerSocket`.

![Ilustración protocolos de aplicación](img/protocolos-de-aplicacion.png)

Como recordarás, un socket Java es la representación de una conexión para la transmisión de información entre dos ordenadores distintos o entre un ordenador y él mismo. Esta abstracción de medio nivel, permite despreocuparse de lo que está pasando en capas inferiores.

Dentro de una red, **un socket** es único pues viene caracterizado por **cinco parámetros**: el protocolo usado (HTTP, FTP, etc.), dos direcciones IP (la del equipo local o donde reside el proceso cliente, y la del equipo remoto o donde reside el proceso servidor) y dos puertos (uno local y otro remoto).

Te recordamos los **pasos que se siguen para establecer, mantener y cerrar una conexión TCP/IP**:

1. Se crean los sockets en el cliente y el servidor
2. El servidor establece el puerto por el que proporciona el servicio
3. El servidor permanece a la escucha de las peticiones de los clientes. 
4. Un cliente conecta con el servidor.
5. El servidor acepta la conexión.
6. Se realiza el intercambio de datos.
7. El cliente o el servidor, o ambos, cierran la conexión.

![Ilustración Conexión y Transmisión](img/conexion-y-transmision.png)

## DNS y resolución de nombres

Todas las computadoras y dispositivos conectados a una red TCP/IP se identifican mediante una dirección IP, como por ejemplo `117.142.234.125`.

En su forma actual (IPv4), una dirección IP se compone de cuatro bytes sin signo (de 0 a 254) separados por puntos para que resulte más legible, tal y como has visto en el ejemplo anterior. Por supuesto, se trata de valores ideales para los ordenadores, pero para los seres humanos que quieran acordarse de la IP de un nodo en concreto de la red, son todo un problema de memoria.

El sistema DNS o Sistema de Nombres de Dominio es el mecanismo que se inventó, para que los nodos que ofrecen algún tipo de servicio interesante tengan un nombre fácil de recordar, lo que se denomina un
nombre de dominio, como por ejemplo `www.todofp.es`.

![Ilustración DNS lookup](img/DNS-lookup.png)

DNS es un sistema de nomenclatura jerárquica para computadoras, servicios o cualquier recurso conectado a Internet o a una red privada.

> El objetivo principal de los nombres de dominio y del servicio DNS, es traducir o resolver nombres (por ejemplo www.dominio.es) en las direcciones IP (identificador binario) de cada equipo conectado a la red, con el propósito de poder localizar y direccionar estos equipos dentro de la red.

Sin la ayuda del servicio DNS, los usuarios de una red TCP/IP tendrían que acceder a cada servicio de la misma utilizando la dirección IP del nodo.

Además el servicio **DNS proporciona otras ventajas**:

* Permite que una misma dirección IP sea compartida por varios dominios.
* Permite que un mismo dominio se corresponda con diferentes direcciones IP.
* Permite que cualquier servicio de red pueda cambiar de nodo, y por tanto de IP, sin cambiar de nombre de dominio.

## El protocolo FTP

El protocolo FTP o Protocolo de Transferencia de Archivos proporciona un mecanismo estándar de transferencia de archivos entre sistemas a través de redes TCP/IP.

Las **principales prestaciones** de un servicio basado en el protocolo FTP o servicio FTP son las siguientes:

* Permite el **intercambio de archivos** entre máquinas remotas a través de la red.

* Consigue una conexión y una **transferencia de datos muy rápidas**.

Sin embargo, **el servicio FTP adolece de una importante deficiencia en cuanto a seguridad**:

* La información y contraseñas se transmiten en   texto plano. Esto está diseñado así, para conseguir una mayor velocidad de transferencia. Al **realizar la transmisión en texto plano**, un posible atacante puede capturar este tráfico, acceder al servidor y/o apropiarse de los archivos transferidos.

Este problema de seguridad, se puede solucionar mediante la   encriptación de todo el tráfico de información, a través del protocolo no estándar SFTP usando SSH o del protocolo FTPS usando SSL. De encriptación ya hablaremos más adelante, en otra unidad de este módulo.

**¿Cómo funciona el servicio FTP?** Es una servicio basado en una arquitectura cliente/servidor y, como tal, seguirá las pautas generales de funcionamiento de este modelo, en ese caso:

* **El servidor** proporciona su servicio a través de dos puertos:
	* **El puerto `20`, para transferencia de datos**.
	* **El puerto `21`, para transferencia de órdenes de control**, como conexión y desconexión.

* **El cliente** se conecta al servidor haciendo uso de un **puerto local mayor de `1024`** y tomando como **puerto destino del servidor el `21`**.

![Ilustración Protocolo FTP](img/protocolo-ftp.png)

**Las principales características del servicio FTP** son las siguientes:

* La conexión de un **usuario** remoto al servidor FTP puede hacerse como: usuario del sistema (debe tener una cuenta de acceso), usuario genérico (utiliza la cuenta anonymous, esto es, usuario anónimo), usuario virtual (no requiere una cuenta local del sistema.

* El acceso al **sistema de archivos** es más o menos limitado, según el tipo de usuario que se conecte y sus privilegios. Por ejemplo, el usuario anónimo solo tendrá acceso al directorio público que establece el administrador por defecto.

* El servicio FTP **soporta dos modos de conexión**: modo activo y modo pasivo.

	* **Modo activo**. Es la forma nativa de funcionamiento (ilustración anterior).
Se establecen dos conexiones distintas: la petición de transferencia por parte del cliente y la atención a dicha petición, iniciada por el servidor. De manera que, si el cliente está protegido con un cortafuegos, deberá configurarlo para que permita esta petición de conexión entrante a través de un puerto que, normalmente, está cerrado por motivos de seguridad.

	* **Modo pasivo**. El cliente sigue iniciando la conexión, pero el problema del cortafuegos se traslada al servidor.

En el siguiente [recurso adicional](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_archivos) podrás seguir profundizando en el protocolo FTP.

## Los protocolos SMTP y POP3

**El correo electrónico es un servicio que permite a los usuarios enviar y recibir mensajes y archivos rápidamente a través de la red**.

* Principalmente se usa este nombre para denominar al sistema que provee este servicio en Internet, mediante el protocolo SMTP o Protocolo Simple de Transferencia de Correo, aunque por extensión también puede verse aplicado a sistemas análogos que usen otras tecnologías.

* Por medio de mensajes de correo electrónico se puede enviar, no solamente texto, sino todo tipo de documentos digitales.

El servicio de correo basado en el protocolo SMTP sigue el modelo cliente/servidor, por lo que el trabajo se distribuye entre el programa Servidor y el Cliente. Te indicamos a continuación, **algunas consideraciones importantes sobre el servicio de correo a través de SMTP**:

* El servidor mantiene las cuentas de los usuarios así como los   buzones correspondientes.
* Los clientes de correo gestionan la descarga de mensajes así como su elaboración.
* El servicio SMTP utiliza el puerto 25.
* El protocolo SMTP se encarga del transporte del correo saliente desde la máquina del usuario remitente hasta el servidor que almacene los mensajes de los destinatarios.
	* El usuario remitente redacta su mensaje y lo envía hacia su servidor de correo.
	* Desde allí se reenvía al servidor del destinatario, quién lo descarga del buzón en la máquina local mediante el protocolo POP3, o la consulta vía web, haciendo uso del protocolo IMAP.

![Ilustración SMTP y POP3](img/smtp.png)

**¿Cómo funciona SMTP y qué mensajes intercambian cliente y servidor?** En el siguiente [recurso adicional](https://es.wikipedia.org/wiki/Protocolo_para_transferencia_simple_de_correo#Funcionamiento) dispones de una explicación del funcionamiento del protocolo SMTP en líneas generales (Resumen simple de funcionamiento).

## El protocolo HTTP

El protocolo HTTP o Protocolo de Transferencia de Hipertexto es un conjunto de normas que posibilitan la comunicación entre servidor y cliente, permitiendo la transmisión de información entre ambos. La información transferida son las conocidas páginas HTML o páginas web. Se trata del método más común de intercambio de información en la World Wide Web.

HTTP define tanto la sintaxis como la semántica que utilizan clientes y servidores para comunicarse. Algunas consideraciones importantes sobre HTTP son las siguientes:

* Es un protocolo que sigue el esquema petición-respuesta entre un cliente y un servidor.

* Utiliza por defecto el puerto 80

* Al cliente que efectúa la petición, por ejemplo un navegador web, se le conoce como agente del usuario (user agent).

* A la información transmitida se la llama recurso y se la identifica mediante un localizador uniforme de recursos (URL).
Por ejemplo: http://www.iesalandalus.org/organizacion.htm

* Los recursos pueden ser archivos, el resultado de la ejecución de un programa, una consulta a una base de datos, la traducción automática de un documento, etc.

El funcionamiento esquemático del protocolo HTTP es el siguiente:

* El usuario especifica en el cliente web la dirección del recurso a localizar con el siguiente formato: `http://dirección[:puerto][ruta]`, por ejemplo: `http://www.iesalandalus.org/organizacion.htm`

* El cliente web descompone la información de la URL diferenciando el protocolo de acceso, la IP o nombre de dominio del servidor, el puerto y otros parámetros si los hubiera.

* El cliente web establece una conexión al servidor y solicita el recurso web mediante un mensaje al servidor, encabezado por un método, por ejemplo `GET /organizacion.htm HTTP/1.1` y otras líneas.

* El servidor contesta con un mensaje encabezado con la línea `HTTP/1.1 200 OK`, si existe la página y la envía, o bien envía un código de error.

* El cliente web interpreta el código HTML recibido. 

* Se cierra la conexión.

En el siguiente [recurso adicional](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto#Ejemplo_de_un_di.C3.A1logo_HTTP) dispones de un ejemplo de diálogo entre cliente y servidor utilizando el protocolo HTTP. Más adelante, estudiaremos algunos detalles más de este protocolo para poder programar un servicio web básico.

**HTTP es un protocolo sin estado**, lo que significa que no recuerda nada relativo a conexiones anteriores a la actual. Algunas aplicaciones necesitan que se guarde el estado y para ello hacen uso de lo que se conoce como *cookie*. Consulta el siguiente [recurso adicional](https://es.wikipedia.org/wiki/Cookie_(informática)#Prop.C3.B3sito) para extender la información sobre ellas.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)