# Programación de servidores

Entre los diferentes aspectos que se deben tener en cuenta cuando se diseña o se programa un servidor o servicio en red, vamos a resaltar los siguientes:

* El servidor debe poder **atender a multitud de peticiones que pueden ser concurrentes en el tiempo**. Esto lo podemos conseguir mediante la programación del servidor utilizando hilos o Threads.

* Es importante **optimizar el tiempo de respuesta del servidor**. Esto lo podemos controlar mediante la monitorización de los tiempos de proceso y transmisión del servidor.

La clase `ServerSocket` es la que se utiliza en Java a la hora de crear servidores. Para programar servidores o servicios basados en protocolos del nivel de aplicación, como por ejemplo el protocolo HTTP, será necesario conocer el comportamiento y funcionamiento del protocolo de aplicación en cuestión, y saber que tipo de mensajes intercambia con el cliente ante una solicitud o petición de datos.

En los siguientes apartados vamos a ver el ejemplo de **cómo programar un servidor web básico**, al que después le añadiremos la funcionalidad de que pueda atender de manera concurrente a varios usuarios, optimizando los recursos.

## Programación de un servidor HTTP

Antes de lanzarnos a la programación del servidor HTTP, vamos a recordar o conocer cómo funciona este protocolo y a sentar las hipótesis de trabajo.

El servidor que vamos a programar cumple lo siguiente:

* Se basa en la versión `1.1` del **protocolo HTTP**.
* Implementará **solo una parte** del protocolo.
* Se basa en **dos tipos de mensajes**: peticiones de clientes a servidores y respuestas de servidores a clientes.
* Nuestro servidor **solo implementará peticiones `GET`**.

¿Cómo procesa el servidor web las peticiones del cliente? Dependerá del método utilizado en la petición por el cliente, que en nuestro ejemplo es el GET. Consulta el siguiente recurso adicional para ver cómo hacerlo.

![Ilustración portada presentación Flash](doc/PSP05_AUXR39/captura.png)
[SWF](doc/PSP05_AUXR39/flash.swf) [WEB](doc/PSP05_AUXR39/flash.html) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-ejemplosserviciosdered/src/master/src/main/java/psp/sampleservidorhttp/)

Para crear un servidor HTTP o servidor web, el esquema básico a seguir será:

* **Crear un `SocketServer`** asociado al puerto `80` (puerto por defecto para el protocolo HTTP). Esperar peticiones del cliente.
* **Acceptar la petición** del cliente.
* **Procesar petición** (intercambio de mensajes según protocolo + transmisión de datos).
* **Cerrar socket** del cliente.

A continuación, vamos a programar un sencillo servidor web que acepte peticiones por el puerto `8066` de un cliente que será tu propio navegador web. Según la URL que incluyas en el navegador, el servidor contestará con diferente información. Los casos que vamos a contemplar son los siguientes:

1. Al poner en tu navegador `http://locahost:8066`, te dará la bienvenida.
2. Al poner en tu navegador `http://localhost:8066/quijote`, mostrará un párrafo de el Quijote.
3. Al poner en tu navegador una URL diferente a las anteriores, como por ejemplo `http://localhost:8066/a`, mostrara un mensaje de error.

**Recuerda detener o parar el servidor, una vez lo hayas probado, antes de volver reiniciarlo**.

## Implementar comunicaciones simultáneas

Para proporcinar la funcionalidad a nuestro servidor, de que pueda atender comunicaciones simultáneas es necesario utilizar hilos o threads.

Un servidor HTTP realista tendrá que atender varias peticiones simultáneamente. Para ello, tenemos que ser capaces de modificar su código para que pueda utilizar varios hilos de ejecución. Esto se hace de la siguiente manera:

* El hilo principal (o sea, el que inicia la aplicación) creará el socket servidor que permanecerá a la espera de que llegue alguna
petición.

* Cuando se reciba una, la aceptará y le asignará un socket cliente para enviarle la respuesta. Pero en lugar de atenderla él mismo, el hilo principal creará un nuevo hilo para que la despache por el socket cliente que le asignó. De esta forma, podrá seguir a la espera de nuevas peticiones.

Esquemáticamente, **el código del hilo principal tendrá el siguiente aspecto**:

```java
socServidor = new ServerSocket(puerto);
while (true) {
	//acepta una petición, y le asigna un socket cliente para la respuesta 
	socketCliente = socServidor.accept();
	
	//crea un nuevo hilo para despacharla por el socketCliente que le asignó 
	hilo = new HiloDespachador(socketCliente);
	hilo.start();
}
```

donde la clase `HiloDespachador` será una extensión de la clase `Thread` de Java, cuyo constructor almacenará el `socketCliente` que recibe en una variable local utilizada luego por su método `run()` para tramitar la respuesta:

```
class HiloDespachador extends Thread {
	private socketCliente;
	
	public HiloDespachador(Socket socketCliente) { 
		this.socketCliente = socketCliente;
	}
	
	public void run() {
		try{
			//tramita la petición por el socketCliente
		} catch (IOException ex) {} 
	}
}
```

Hecho esto, tendremos todo un **Servidor HTTP capaz de gestionar peticiones concurrentes** de manera más eficiente.

## Monitorización de tiempos de respuesta

Una cuestión muy importante para evaluar el rendimiento y comportamiento de una aplicación cliente/servidor es monitorizar los tiempos de respuesta del servidor. **Los tiempos que intervienen desde que un cliente realiza una petición al servidor hasta que recibe su resultado son dos**:

* **Tiempo de procesamiento**. Es el tiempo que el servidor necesita para procesar la petición del cliente y enviar los datos.
* **Tiempo de transmisión**. Es el tiempo que tardan los mensajes en llegar a su destino a través de la red.

¿Cómo podemos medir esos tiempos?

* Para **medir el tiempo de procesamiento** bastar medir el tiempo que transcurre en el servidor para procesar la solicitud del cliente. Por ejemplo, el siguiente código permite medir ese tiempo en milisegundos:

```java
// anota la lectura del reloj del sistema antes de iniciar el procesamiento
long tiempoInicio = System.currentTimeMillis();

// anota la lectura del reloj del sistema al finalizar el procesamiento
long tiempoFin = System.currentTimeMillis();
long tiempoProceso = tiempoFin - tiempoInicio;
System.out.println("Tiempo en procesar petición: " + tiempoProceso);
```

* Para **medir el tiempo de transmisión** será necesario que el servidor envíe un mensaje con el tiempo del sistema al cliente. El cliente al recibir el mensaje debe calcular su tiempo de sistema y compararlo con el tiempo recibido en el mensaje.

Para poder comparar los tiempos de respuesta de dos equipos **es imprescindible que sus relojes de sistema estén sincronizados** a través de cualquier servicio de tiempo ([*NTP*](https://es.wikipedia.org/wiki/Network_Time_Protocol)). En equipos Windows por ejemplo, la sincronización de los relojes se realiza automáticamente.

### Ejemplo de monitorización del tiempo de transmisión

Vamos a ver un ejemplo de cómo calcular el tiempo de transmisión de un recurso desde un servidor web. Para ello, vamos a modificar el cliente HTTP realizado anteriormente con las clases `URL` y `URLConnection`, para añadir la función `URLConnection.getDate()` que proporciona el instante en el que el servidor inicia la transmisión de su respuesta al cliente. Ese tiempo es enviado por el servidor al cliente mediante la cabecera `Date`, al igual que la cabecera que vimos anteriormente sobre el tipo de contenido, y que se podía recuperar con el método `URLConnection.getContentType()`.

En el siguiente [recurso adicional](https://bitbucket.org/eduxunta/edu-java-psp-ejemplosserviciosdered/src/master/src/main/java/psp/sampletiempotransmision/) encontrarás el código Java completo.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)