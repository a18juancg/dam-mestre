# Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte.

Derechos de propiedad intelectual y de propiedad industrial ©

[![Logo Ministerio de Educación y Formación Profesional](http://www.educacionyfp.gob.es/docroot/utils/img/escudo-mefp-mcd.gif)](http://www.educacionyfp.gob.es/portada.html)

[Aviso Legal](http://www.educacionyfp.gob.es/aviso-legal-mecd/)

> Se autoriza la reproducción total o parcial de los textos proporcionados por el portal, siempre que se mantenga su integridad y que se cite expresamente al Ministerio de Educación y Formación Profesional como fuente de la información.