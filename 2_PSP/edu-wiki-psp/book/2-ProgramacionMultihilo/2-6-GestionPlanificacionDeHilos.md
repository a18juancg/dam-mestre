# Gestión y planificación de hilos

La ejecución de hilos se puede realizar mediante:

* **Paralelismo**. En un sistema con múltiples CPU, cada CPU puede ejecutar un hilo diferente.

* **Pseudoparalelismo**. Si no es posible el paralelismo, una CPU es responsable de ejecutar múltiples hilos.

La ejecución de múltiples hilos en una sola CPU requiere la planificación de una secuencia de ejecución (scheduling).

![Ilustración Monocore vs Multicore](img/gestionYPlanificacionHilos.png)

**El planificador de hilos de Java (Scheduler)** utiliza un algoritmo de secuenciación de hilos denominado fixed priority scheduling que está basado en un sistema de prioridades relativas, de manera que el algoritmo secuencia la ejecución de hilos en base a la prioridad de cada uno de ellos.

El **funcionamiento** del algoritmo es el siguiente:

* El hilo elegido para ejecutarse, siempre es el hilo "Ejecutable" de prioridad más alta.

* Si hay más de un hilo con la misma prioridad, el orden de ejecución se maneja mediante un algoritmo por turnos (round-robin) basado en una cola circular FIFO (Primero en entrar, primero en salir).

* Cuando el hilo que está "ejecutándose" pasa al estado de "No Ejecutable" o "Muerto", se selecciona otro hilo para su ejecución.

* La ejecución de un hilo se interrumpe, si otro hilo con prioridad más alta se vuelve "Ejecutable". El hecho de que un hilo con una prioridad más alta interrumpa a otro se denomina **"planificación apropiativa" ('preemptive sheudling').**

Pero la responsabilidad de ejecución de los hilos es del Sistemas Operativos sobre el que corre la JVM, y **Sistemas Operativos distintos manejan los hilos de manera diferente**:

* En un Sistema Operativo que implementa *time-slicing* (subdivisión de tiempo), el hilo que entra en ejecución, se mantiene en ella sólo un micro-intervalo de tiempo fijo o cuanto (quantum) de procesamiento, de manera que el hilo que está "ejecutándose" no solo es interrumpido si otro hilo con prioridad más alta se vuelve "Ejecutable", sino también cuando su "cuanto" de ejecución se acaba. Es el patrón seguido por Linux, y por todos los Windows a partir de Windows 95 y NT.

* En un Sistema Operativo que no implementa *time-slicing* el hilo que entra en ejecución, es ejecutado hasta su muerte; salvo que regrese a "No ejecutable", u otro hilo de prioridad más alta alcance el estado de "Ejecutable" (en cuyo caso, el primero regresa a "preparado" para que se ejecute el segundo). **Es el patrón seguido en el Sistema Operativo Solaris**.

## Prioridad de hilos

En Java, cada hilo tiene una prioridad representada por un valor de tipo entero entre 1 y 10. Cuanto mayor es el valor, mayor es la prioridad del hilo.

Por defecto, el **hilo principal** de cualquier programa, o sea, el que ejecuta su método main() siempre es **creado con prioridad 5**.

El resto de **hilos secundarios** (creados desde el hilo principal, o desde cualquier otro hilo en funcionamiento), **heredan la prioridad que tenga en ese momento su hilo padre**.

En la clase `Thread` se definen 3 constantes para manejar estas prioridades:

* `MAX_PRIORITY` (= 10). Es el valor que simboliza la máxima prioridad.

* `MIN_PRIORITY` (=1). Es el valor que simboliza la mínima prioridad.

* `NORM_PRIORITY` (= 5). Es el valor que simboliza la prioridad normal, la que tiene por defecto el hilo donde corre el método `main()`.

Además en cualquier momento se puede **obtener y modificar la prioridad de un hilo**, mediante los siguientes métodos de la clase `Thread`:

* `getPriority()`. Obtiene la prioridad de un hilo. Este método devuelve la prioridad del hilo.

* `setPriority()`. Modifica la prioridad de un hilo. Este método toma como argumento un entero entre 1 y 10, que indica la nueva prioridad del hilo.

Java tiene 10 niveles de prioridad que no tienen por qué coincidir con los del sistema operativo sobre el que está corriendo. Por ello, lo mejor es que utilices en tu código sólo las constantes `MAX_PRIORITY`, `NORM_PRIORITY` y `MIN_PRIORITY`.

Podemos conseguir aumentar el rendimiento de una aplicación multihilo gestionando adecuadamente las prioridades de los diferentes hilos, por ejemplo utilizando una prioridad alta para tareas de tiempo crítico y una prioridad baja para otras tareas menos importantes.

En el siguiente [ejemplo](https://bitbucket.org/eduxunta/edu-java-psp-ejemploshilos/src/master/src/main/java/psp/ejemploprioridadhilos/) en el que se declara un hilo cuya tarea es llenar un vector con 20.000 caracteres. Se inician 15 hilos con prioridades diferentes, 5 con prioridad máxima, 5 con prioridad normal y 5 con prioridad mínima. Al ejecutar el programa comprobarás que los hilos con prioridad más alta tienden a finalizar antes. Observa que se usa también el método `yield()` del que hablaremos en el siguiente apartado.

## Hilos egoístas y programación expulsora

¿De verdad existen los hilos egoístas? En un Sistema Operativo que no implemente time-slicing puede ocurrir que un hilo que entra en "ejecución" no salga de ella hasta su muerte, de manera que no dará ninguna posibilidad a que otros hilos "preparados" entren en "ejecución" hasta que él muera. Este hilo se habrá convertido en un **hilo egoísta**.

![Ilustración Hilo Egoista](img/hiloEgoista.png)

Por ejemplo, supongamos la siguiente situación en un Sistema Operativo **sin time-slicing**:

* La tarea asociada al método run() de un hilo consiste en imprimir 100 veces la palabra que se le pasa al constructor más el número de orden.

* Se inician dos hilos en main(), uno imprimiendo "Azul" y otro "Rojo".

* El hilo que sea seleccionado en primer lugar por el planificador se ejecutará íntegramente, por ejemplo el que imprime "Rojo" 100 veces. Después se ejecutará el otro hilo, tal y como muestra la imagen parcial de la derecha.

* Este hilo tiene un **comportamiento egoísta**.

![Ilustración Traza Hilo Egoista](img/trazaHiloEgoista.png)

En un Sistema Operativo que si implemente time-slicing la ejecución de esos hilos se entremezcla, tal y como muestra la imagen parcial de la izquierda, lo cual indica que no hay comportamiento egoísta de ningún hilo, esto es, el Sistema operativo combate los hilos egoístas.

![Ilustración Traza Hilo No Egoista](img/trazaHiloNoEgoista.png)

El proyecto completo lo puedes descargar desde el [siguiente enlace](https://bitbucket.org/eduxunta/edu-java-psp-ejemploshilos/src/master/src/main/java/psp/ejemploposiblehiloegoista/).

Según lo anterior, un mismo programa Java se puede ejecutar de diferente manera según el Sistema Operativo donde corra. Entonces, ¿que pasa con la   portabilidad de Java? Java da solución a este problema mediante lo que se conoce como programación expulsora a través del método `yield()` de la clase `java.lang.Thread`:

* `yield()` hace que un hilo que está "ejecutándose" pase a "preparado" **para permitir que otros hilos de la misma prioridad puedan ejecutarse**.

Sobre el método `yield()` y el egoísmo de los threads debes tener en cuenta que:

* El funcionamiento de `yield()` no está garantizado, puede que después de que un hilo invoque a `yield()` y pase a "preparado", éste vuelva a ser elegido para ejecutarse.

* **No debes asumir que la ejecución de una aplicación se realizará en un Sistema Operativo que implementa time-slicing**.

* En la aplicación debes incluir adecuadamente llamadas al método `yield()`, incluso a `sleep()` o `wait()`, si el hilo no se bloquea por una Entrada/Salida.

El siguiente código muestra la forma de invocar a `yield()` dentro del método `run()` de un hilo. Ten en cuenta que si la invocación se hace desde un hilo `Runnable` tendrás que poner `Thread.yield()`:

```java
public void run(){
	for(int i=1; i<=100; i++){
		System.out.println(color +1);
		yield(); //también Thread.yield()
	}
}
```

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)