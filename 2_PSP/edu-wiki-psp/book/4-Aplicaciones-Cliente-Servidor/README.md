# Índice

1. [Paradigma Cliente/Servidor](4-1-ParadigmaClienteServidor.md)
2. [Optimización de sockets](4-2-OptimizacionDeSockets.md)

## Mapa conceptual

![Ilustración del Mapa conceptual](img/PSP04_MapaConceptual.png)

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)