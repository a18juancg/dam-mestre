# Paradigma Cliente/Servidor

En el mundo de las comunicaciones entre equipos el modelo de comunicación más utilizado es el   modelo Cliente/servidor ya que ofrece una gran flexibilidad, interoperabilidad y estabilidad para acceder a recursos de forma centralizada.

El término modelo Cliente/Servidor se acuñó por primera vez en los años 80 para explicar un sencillo paradigma: un equipo cliente requiere un servicio de un equipo servidor.

**Desde el punto de vista funcional, se puede definir el modelo Cliente/Servidor como una arquitectura distribuida que permite a los
usuarios finales obtener acceso a recursos de forma transparente en entornos multiplataforma.** Normalmente, los recursos que suele ofrecer el servidor son datos, pero también puede permitir acceso a dispositivos hardware, tiempo de procesamiento, etc.

Los elementos que componen el modelo son:

* **Cliente**. Es el proceso que **permite interactuar con el usuario, realizar las peticiones, enviarlas al servidor y mostrar los datos al cliente**. En definitiva, se comporta como la interfaz (   front-end) que utiliza el usuario para interactuar con el servidor. Las funciones que lleva a cabo el proceso cliente se resumen en los siguientes puntos:

	* Interactuar con el usuario.
	* Procesar las peticiones para ver si son válidas y evitar peticiones maliciosas al servidor. 
	* Recibir los resultados del servidor.
	* Formatear y mostrar los resultados.

* **Servidor**. Es el **proceso encargado de recibir y procesar las peticiones de los clientes** para permitir el acceso a algún recurso (*back-end*). Las funciones del servidor son:

	* Aceptar las peticiones de los clientes.
	* Procesar las peticiones.
	* Formatear y enviar el resultado a los clientes.
	* Procesar la lógica de la aplicación y realizar validaciones de datos. 
	* Asegurar la consistencia de la información.
	* Evitar que las peticiones de los clientes interfieran entre sí. 
	* Mantener la seguridad del sistema.

**La forma más habitual de utilizar el modelo cliente/servidor es mediante la utilización de equipos a través de interfaces gráficas**; mientras que la administración de datos y su seguridad e integridad se deja a cargo del servidor. *Normalmente*, el trabajo pesado lo realiza el servidor y los procesos clientes sólo se encargan de interactuar con el usuario. En otras palabras, el modelo Cliente/Servidor es una extensión de programación modular en la que se divide la funcionalidad del software en dos módulos con el fin de hacer más fácil el desarrollo y mejorar su mantenimiento.

## Características básicas

* Combinación de un **cliente que interactúa** con el usuario, y un **servidor que interactúa con los recursos compartidos**. El proceso del cliente proporciona la interfaz de usuario y el proceso del servidor permite el acceso al recurso compartido.

* Las tareas del cliente y del servidor **tienen diferentes requerimientos en cuanto al procesamiento**; todo el trabajo de procesamiento lo
realiza el servidor y mientras que el cliente interactúa con el usuario.

* **Se establece una relación entre distintos procesos**, las cuales se
pueden ejecutar en uno o varios equipos distribuidos a lo largo de la red.

* Existe una clara distinción de funciones basada en el concepto de "*servicio*", que se establece entre clientes y servidores.

* **La relación establecida puede ser de muchos a uno**, en la que un servidor puede dar servicio a muchos clientes, regulando el acceso a los recursos compartidos.

* Los clientes corresponden a **procesos activos** ya que realizan las peticiones de servicios a los servidores. Estos últimos tienen un **carácter pasivo** ya que esperan las peticiones de los clientes.

* Las comunicaciones se realizan estrictamente a través del **intercambio de mensajes**.

* **Los clientes pueden utilizar sistemas heterogéneos** ya que permite conectar clientes y servidores independientemente de sus plataformas.

## Ventajas y desventajas

¿Te has preguntado alguna vez qué ventajas y desventajas el esquema Cliente/Servidor? ¡Veámoslas!

Entre las principales *ventajas* del esquema Cliente/Servidor destacan:

* Utilización de **clientes ligeros** (con pocos requisitos hardware) ya que el servidor es quien realmente realiza todo el procesamiento de la información.

* **Facilita la integración** entre sistemas diferentes y comparte información permitiendo interfaces amigables al usuario.

* Se favorece la utilización de interfaces gráficas interactivas para los clientes para interactuar con el servidor. El uso de interfaces gráficas en el
modelo Cliente/Servidor presenta la ventaja, con respecto a un sistema centralizado, de que **normalmente sólo transmite los datos por lo que se aprovecha mejor el ancho de banda de la red.**

* **El mantenimiento y desarrollo de aplicaciones resulta rápido** utilizando las herramientas existentes. 

* La estructura inherentemente modular facilita además la integración de nuevas tecnologías y el crecimiento de la infraestructura computacional, favoreciendo así la **escalabilidad** de las soluciones. 

* **Contribuye a proporcionar a los diferentes departamentos de una organización**, soluciones locales, pero permitiendo la integración de la información relevante a nivel global.

* El **acceso a los recursos** se encuentra **centralizado**.

* Los clientes **acceden de forma simultánea** a los datos compartiendo información entre sí.

Entre las principales *desventajas* del esquema Cliente/Servidor destacan:

* El mantenimiento de los sistemas es más difícil pues implica la interacción de diferentes partes de hardware y de software lo cual dificulta el **diagnóstico de errores**.

* **Hay que tener estrategias** para el manejo de errores del sistema.

* Es importante **mantener la seguridad** del sistema.

* Hay que garantizar la **consistencia de la información**. Como es posible que varios clientes operen con los mismos datos de forma simultánea, es necesario utilizar mecanismos de sincronización para evitar que un cliente modifique datos sin que lo sepan los demás clientes.

## Modelos

La principal forma de clasificar los modelos Cliente/Servidor es a partir del número de capas (tiers) que tiene la infraestructura del sistema. De ésta forma podemos tener los siguientes modelos:

* **1 capa (1-tier)**. El proceso cliente/servidor se encuentra en el mismo equipo y realmente no se considera un modelo cliente/servidor ya que no se realizan comunicaciones por la red.

* **2 capas (2-tiers)**. Es el modelo tradicional en el que existe un servidor y unos clientes bien diferenciados. El principal problema de éste modelo es que no permite escalabilidad del sistema y puede sobrecargarse con un número alto de peticiones por parte de los clientes.

![Ilustración modelo 2 capas](img/2tier.png)

* **3 capas (3-tiers)**. Para mejorar el rendimiento del sistema en el modelo de dos capas se añade una nueva capa de servidores. En este caso se dispone de:

	* *Servidor de aplicación*. Es el encargado de interactuar con los diferentes clientes y enviar las peticiones de procesamiento al servidor de datos.
	
	* *Servidor de datos*. Recibe las peticiones del servidor de aplicación, las procesa y le devuelve su resultado al servidor de aplicación para que éste los envíe al cliente. Para mejorar el rendimiento del sistema, es posible añadir los servidores de datos que sean necesarios.

![Ilustración modelo 3 capas](img/3tier.png)

* **N capas (n-tiers)**. A partir del modelo anterior, se pueden añadir capas adiciones de servidores con el objetivo de separar la funcionalidad de cada servidor y de mejorar el rendimiento del sistema.

## Programación

De forma interna, **los pasos que realiza el servidor** para realizar una comunicación son:

* **Publicar puerto**. Publica el puerto por donde se van a recibir las conexiones.

* **Esperar peticiones**. En este momento el servidor queda a la espera a que se conecte un cliente. Una vez que se conecte un cliente se crea el socket del cliente por donde se envían y reciben los datos. 

* **Envío y recepción de datos**. Para poder recibir/enviar datos es necesario crear un flujo (*stream*) de entrada y otro de salida. Cuando el servidor recibe una petición, éste la procesa y le envía el resultado al cliente.

* Una vez finalizada la comunicación **se cierra el socket del cliente**.

**Los pasos que realiza el cliente** para realizar una comunicación son:

* **Conectarse con el servidor**. El cliente se conecta con un determinado servidor a un puerto específico. Una vez realizada la conexión se crea el socket por donde se realizará la comunicación.

* **Envío y recepción de datos**. Para poder recibir/enviar datos es necesario crear un flujo (stream) de entrada y otro de salida.

* Una vez finalizada la comunicación **se cierra el socket**.

![Ilustración del Esquema de funcionamiento tinterno del modelo cliente/servidor](img/esquemaDelFuncionamientoInterno.png)

## Ejemplo (Concurrencia de peticiones secuencial)

A modo de ejemplo, vamos a ver un ejemplo sencillo en el que el servidor va a aceptar tres clientes (de forma secuencial no concurrente) y le va a indicar el número de cliente que es.

#### Servidor.java

```java
import java.io.*;
import java.net.*;

class Servidor {
	static final int Puerto = 2000;

	public Servidor() {
		try {
			ServerSocket sServidor = new ServerSocket(Puerto);
			System.out.println("Escucho el puerto " + Puerto);
			for (int nCli = 0; nCli < 3; nCli++) {
				Socket sCliente = sServidor.accept();
				System.out.println("Sirvo al cliente " + nCli);
				DataOutputStream flujo_salida = new DataOutputStream(sCliente.getOutputStream());
				flujo_salida.writeUTF("Hola cliente " + nCli);
				sCliente.close();
			}
			System.out.println("Se han atendido los clientes");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] arg) {
		new Servidor();
	}
}
```

Lógicamente, el funcionamiento del cliente no cambia ya que la concurrencia la realiza el servidor. A continuación puede ver un ejemplo básico de un cliente.

#### Cliente.java

```java
import java.io.*;
import java.net.*;

class Servidor {
	static final int Puerto = 2000;

	public Servidor() {
		try {
			ServerSocket sServidor = new ServerSocket(Puerto);
			System.out.println("Escucho el puerto " + Puerto);
			for (int nCli = 0; nCli < 3; nCli++) {
				Socket sCliente = sServidor.accept();
				System.out.println("Sirvo al cliente " + nCli);
				DataOutputStream flujo_salida = new DataOutputStream(sCliente.getOutputStream());
				flujo_salida.writeUTF("Hola cliente " + nCli);
				sCliente.close();
			}
			System.out.println("Se han atendido los clientes");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] arg) {
		new Servidor();
	}
}
```

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)