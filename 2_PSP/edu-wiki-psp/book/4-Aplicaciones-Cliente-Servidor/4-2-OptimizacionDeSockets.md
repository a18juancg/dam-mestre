# Optimización de Sockets

A la hora de utilizar los sockets es muy importante optimizar su funcionamiento y garantizar la seguridad del sistema. Como la información reside en el servidor y existen múltiples clientes que realizan peticiones **es totalmente indispensable permitir que la aplicación cliente/servidor cuente con las siguientes características** que veremos más adelante:

* **Atender múltiples peticiones simultáneamente**. El servidor debe permitir el acceso de forma simultánea al servidor para acceder a los recursos o servicios que éste ofrece.

* **Seguridad**. Para asegurar el sistema, como mínimo, el servidor debe ser capaz de **evitar la pérdida de información**, **filtrar las peticiones** de los clientes para asegurar que éstas están bien formadas y llevar un control sobre las diferentes transacciones de los clientes.

* Por último, es necesario dotar a nuestro sistema de mecanismos para **monitorizar los tiempos de respuesta** de los clientes para ver el comportamiento del sistema.

## Atender múltiples peticiones simultáneas.

Si observamos la siguiente figura, cuando un servidor recibe la conexión del cliente (`accept`) se crea el socket del cliente, se realiza el envío y recepción de datos y se cierra el socket del cliente finalizando la ejecución del servidor.

**Como el objetivo es permitir que múltiples clientes utilicen el servidor de forma simultánea es necesario que la parte que atiende al cliente (zona coloreada de azul) se atienda de forma independiente para cada uno de los clientes.**

![Ilustración Peticiones Simultáneas 1](img/peticionesSimultaneas1.png)

Para ello, en vez de ejecutar todo el código del servidor de forma secuencial, vamos a tener un bucle while para que cada vez que se realice la conexión de un cliente se cree una hebra de ejecución (thread) que será la encargada de atender al cliente. De ésta forma, tendremos tantas hebras de ejecución como clientes se conecten a nuestro servidor de forma simultánea.

De forma resumida, el código necesario es:

```java
while(true){
	// Se conecta un cliente
	Socket skCliente = skServidor.accept(); 	System.out.println("Cliente conectado"); 
	
	// Atiendo al cliente mediante un thread 
	new Servidor(skCliente).start();
}
```
Y a continuación puede ver su representación de forma gráfica:

![Ilustración Peticiones Simultáneas 2](img/peticionesSimultaneas2.png)

Si deseas más información sobre el uso de threads y sockets en java puedes consultar la siguiente presentación.

![Ilustración de la portada de la presentación](doc/socket_y_threads_en_java/captura.png)
[WEB](doc/socket_y_threads_en_java/flash.html) [SWF](doc/socket_y_threads_en_java/flash.swf)

## Threads (Concurrencia de peticiones)

Para crear una hay que definir la clase que extienda de Threads:

```java
class Servidor extends Thread{
	public Servidor() {
		// Inicialización de la hebra
	}

	public static void main( String[] arg ) { 
		new Servidor().start();
	}
	
   public void run(){
     //tareas que realiza la hebra
   } 
}
```

donde:

* La función `public Servidor` permite inicializar los valores iniciales que recibe la hebra. 
* La función `run()` es la encargada de realizar las tareas de la hebra.
* Para iniciar la hebra se crea el objeto `Servidor` y se inicia: `new Servidor().start();`


## Ejemplo (Concurrente)

Si añade al código anterior la utilización de sockets, tal y como se ha visto anteriormente, por parte del servidor obtiene un servidor que permite atender múltiples peticiones de forma concurrente:

#### Servidor.java

```
import java.io.*;
import java.net.*;

class Servidor extends Thread {
	Socket skCliente;
	static final int Puerto = 2000;

	public Servidor(Socket sCliente) {
		skCliente = sCliente;
	}

	public static void main(String[] arg) {
		try {
			// Inicio el servidor en el puerto
			ServerSocket skServidor = new ServerSocket(Puerto);
			System.out.println("Escucho el puerto " + Puerto);
			while (true) {
				// Se conecta un cliente
				Socket skCliente = skServidor.accept();
				System.out.println("Cliente conectado"); 
				
				// Atiendo al cliente mediante un thread new
				new Servidor(skCliente).start();
			}
		}
		catch (Exception e) {;}
	}

	public void run() {
		try {
			// Creo los flujos de entrada y salida
			DataInputStream flujo_entrada = new DataInputStream(skCliente.getInputStream());
			DataOutputStream flujo_salida = new DataOutputStream(skCliente.getOutputStream());
			// ATENDER PETICIÓN DEL CLIENTE
			flujo_salida.writeUTF("Se ha conectado el cliente de forma correcta");
			// Se cierra la conexión
			skCliente.close();
			System.out.println("Cliente desconectado");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
```

Lógicamente, [el funcionamiento del cliente no cambia](4-1-ParadigmaClienteServidor.md#Cliente.java) ya que la concurrencia la realiza el servidor.

## Pérdida de información

La pérdida de paquetes en las comunicaciones de red es un factor muy importante que hay que tener en cuenta ya que, por ejemplo, si se envía un fichero la pérdida de un único paquete produce que el fichero no se reciba correctamente.

Para evitar la pérdida de paquetes en las comunicaciones, cada vez que se envía un paquete el receptor envía al emisor un **paquete de confirmación** [ACK (acknowledgement)](https://es.wikipedia.org/wiki/ACK).

![Ilustración ACK](img/ack1.png)

En el caso que el mensaje no llegue correctamente al receptor el paquete de confirmación no se envía nunca. El emisor cuando transcurre un determinado tiempo considera que el paquete se ha producido un error y vuelve a enviar el paquete.

Este método, aunque efectivo, resulta **bastante lento** ya que para enviar un nuevo paquete debe esperar el ACK del paquete anterior por lo que se produce un retardo en las comunicaciones.

**Una mejora** importante del método anterior **consiste en permitir al emisor que envíe múltiples paquetes sin necesidad de esperar los paquetes de confirmación**. De esta forma el emisor puede enviar `N` paquetes de forma simultánea y así mejorar las comunicaciones.

![Ilustración ACK](img/ack2.png)

Como una de las características de las redes es que **es posible que los paquetes no lleguen ordenados**, ni los paquetes ACK lleguen ordenados o, simplemente que se pierda algún mensaje en el camino, **es necesario llevar un control sobre los paquetes enviados y los confirmados**.

Para llevar un control de los paquetes enviados **se utiliza un vector** en el que se indica si un determinado paquete se ha enviado correctamente o no. Lógicamente, como los paquetes pueden llegar de forma desordenada, perderse paquetes,... es posible encontrar en el vector de configuración múltiples combinaciones como la siguiente:

![Ilustración Vector de ACK (estado inicial)](img/vectorACKinicial.png)

Como puede ver en el ejemplo anterior, los mensajes `0`, `1`, `4`, y `5` han llegado correctamente. Por lo tanto para poder retransmitir más mensajes se desplaza el vector de derecha a izquierda con los mensajes enviados correctamente hasta llegar al primer mensaje no enviado correctamente (en el ejemplo el `2`). De esta forma siguiendo el ejemplo propuesto el vector queda de la siguiente forma:

![Ilustración Vector de ACK (desplazado)](img/vectorACKdesplazado.png)

Ahora el sistema ya puede enviar los mensajes `10` y `11` mientras que espera la confirmación de los demás mensajes.

Como ha podido observar, **el tamaño del vector influye muy estrechamente en el rendimiento del sistema ya que cuando mayor sea el vector más mensajes se pueden enviar de forma concurrente**. Lógicamente, existe la limitación de la memoria RAM que ocupa el vector.


## Transacciones

Uno de los principales fallos de seguridad que se producen en la programas clientes/servidor es que el cliente pueda realizar:

* **Operaciones no autorizadas**. El servidor no puede procesar una orden a la que el cliente no tiene acceso. Por ejemplo, que el cliente realice una solicitud de información a la que no tiene acceso.

* **Mensajes mal formados**. Es posible que el cliente envíe al servidor mensajes mal formados o con información incompleta que produzca un error de procesamiento del sistema llegando incluso a dejar "colgado" el servidor.

Para evitar cualquier problema de seguridad es muy importante modelar el flujo de información y el comportamiento del servidor con un **diagrama de estados o autómata**. Por ejemplo, en la siguiente figura puede ver que el servidor se inicia en el estado `0` y directamente envía al cliente el mensaje `Introduce el comando`. El cliente puede enviar los comandos:

* **`ls`** que va al estado `2` mostrando el contenido del directorio y vuelve automáticamente al estado `1`.

* **`get`** que le lleva al estado `3` donde le solicita al cliente el nombre del archivo a mostrar. Al introducir el nombre del archivo se desplaza al estado `4` donde muestra el contenido del archivo y vuelve automáticamente al estado `1`.

* **`exit`** que le lleva directamente al estado donde finaliza la conexión del cliente (estado `-1`).

* **Cualquier otro comando** hace que vuelva al estado `1` solicitándole al cliente que introduzca un comando válido.

![Ilustración diagrama de estados o autómata](img/diagramaDeEstados.png)

Para poder seguir el comportamiento del autómata el servidor tiene que definir dos variables `estado` y `comando`. La variable `estado` almacena la posición en la que se encuentra y la variable `comando` es el comando que recibe el servidor y el que permite la transición de un estado a otro. 

Cuando se utilizan autómatas muy sencillos como es el caso del ejemplo, es posible modelar el comportamiento del autómata utilizando estructuras `case` e `if`. En el caso de utilizar autómatas grandes la mejor forma de modelar su comportamiento es mediante una tabla cuyas filas son los diferentes estados del autómata y la columna las diferentes entradas del sistema.

En este [recurso adicional](https://es.wikipedia.org/wiki/Autómata_finito) tienes más información sobre los autómatas finitos.

## Ejemplo Implementación diagrama de estados

A continuación, a modo de ejemplo se muestra la estructura general para implementar el diagrama de transiciones del ejemplo anterior.

```java
int estado = 1;
do {
	
	switch (estado) {
		case 1:
			flujo_salida.writeUTF("Introduce comando (ls/get/exit)");
			comando = flujo_entrada.readUTF();
			if (comando.equals("ls")) {
				System.out.println("\tEl cliente quiere ver el contenido del directorio");
				// Muestro el directorio
				estado = 1;
				break;
			} else if (comando.equals("get")) {
				// Voy al estado 3 para mostrar el fichero estado=3;
				break;
			} else
				estado = 1;
			break;
		case 3:// voy a mostrar el archivo 
			flujo_salida.writeUTF("Introduce el nombre del archivo"); 
			String fichero =flujo_entrada.readUTF();
			// Muestro el fichero
			estado = 1;
			break;
	}
	if (comando.equals("exit"))
		estado = -1;
} while (estado != -1);
```

## Monitorizar tiempos de respuesta

Un aspecto muy importante para ver el comportamiento de nuestra aplicación Cliente/Servidor son los tiempos de respuesta del servidor. Desde que el cliente realiza una petición hasta que recibe su resultado intervienen dos tiempos:

* **Tiempo de procesamiento**. Es el tiempo que el servidor necesita para procesar la petición del cliente y enviar los datos.

* **Tiempo de transmisión**. Es el tiempo que transcurre para que los mensajes viajen a través de los diferentes dispositivos de la red hasta llegar a su destino.

Para medir el tiempo de procesamiento tan sólo se necesitar medir el tiempo que transcurre en que el servidor procese la solicitud del cliente. Para medir el tiempo en milisegundos necesario para procesar la petición de un cliente puede utilizar el siguiente código:

```java
import java.util.Date;

long tiempo1=(new Date()).getTime(); // Procesar la petición del cliente
long tiempo2=(new Date()).getTime();

System.out.println("\t Tiempo = "+(tiempo2-tiempo1)+" ms");
```

Para medir el tiempo de transmisión es necesario enviar a través de un mensaje el tiempo del sistema y el receptor comparar su tiempo de respuesta con el que hay dentro del mensaje. **Lógicamente, para poder comparar los tiempos de respuesta de dos equipos es totalmente necesario que los relojes del sistema estén sincronizados a través de cualquier servicio de tiempo ([NTP](https://es.wikipedia.org/wiki/Network_Time_Protocol))**. En equipos Windows la sincronización de los relojes se realiza automáticamente y en equipos GNU/Linux se realiza ejecutando el siguiente comando:

```bash
/usr/sbin/ntpdate -u 0.centos.pool.ntp.org`
```
Otra forma de calcular el tiempo de transmisión es utilizar el comando `ping`. Como puede ver en la siguiente figura el tiempo para que el cliente origen acceda al servidor `www.google.es` es de unos 13 milisegundos de media.

![Ilustración del comando ping](img/ping.png)

## Ejemplo Tiempos de Transmisión

A continuación vamos a ver un ejemplo en el que se calcula el tiempo de trasmisión de datos entre una aplicación Cliente y Servidor. Para ello, el servidor le va a enviar al cliente un mensaje con el tiempo del sistema en milisegundos y el cliente cuando reciba el mensaje calculará la diferencia entre el tiempo de su sistema y el del mensaje.

#### Servidor.java

```java
import java.io.*;
import java.net.*;
import java.util.Date;

class Servidor {
	static final int Puerto = 2000;

	public Servidor() {
		try {
			// Inicio el servidor en el puerto
			ServerSocket sServidor = new ServerSocket(Puerto);
			System.out.println("Escucho el puerto " + Puerto);
			// Se conecta un cliente
			Socket sCliente = sServidor.accept(); // Crea objeto System.out.println("Cliente conectado");
			
			// Creo los flujos de entrada y salida
			DataInputStream flujo_entrada = new DataInputStream(sCliente.getInputStream());
			DataOutputStream flujo_salida = new DataOutputStream(sCliente.getOutputStream());
			
			// CUERPO DEL ALGORITMO
			long tiempo1 = (new Date()).getTime();
			flujo_salida.writeUTF(Long.toString(tiempo1));
			
			// Se cierra la conexión
			sCliente.close();
			System.out.println("Cliente desconectado");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public static void main(String[] arg) {
		new Servidor();
	}
}
```

#### Cliente.java

```java
import java.io.*;
import java.net.*;
import java.util.Date;

class Cliente {
	static final String HOST = "localhost";
	static final int Puerto = 2000;

	public Cliente() {
		String datos = new String();
		String num_cliente = new String();
		// para leer del teclado
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			// Me conecto al puerto
			Socket sCliente = new Socket(HOST, Puerto);
			// Creo los flujos de entrada y salida
			DataInputStream flujo_entrada = new DataInputStream(sCliente.getInputStream());
			DataOutputStream flujo_salida = new DataOutputStream(sCliente.getOutputStream());
			// CUERPO DEL ALGORITMO
			datos = flujo_entrada.readUTF();
			long tiempo1 = Long.valueOf(datos);
			long tiempo2 = (new Date()).getTime();
			System.out.println("\n El tiempo es:" + (tiempo2 - tiempo1));
			// Se cierra la conexión
			sCliente.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public static void main(String[] arg) {
		new Cliente();
	}
}
```

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)