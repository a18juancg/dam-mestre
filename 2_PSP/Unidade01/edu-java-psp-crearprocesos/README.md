# README

## Para qué es este repo?

Este repo plantea un sencillo ejemplo de como lanzar nuevos procesos desde Java.

Se recomienda revisar [los contenidos](https://bitbucket.org/eduxunta/edu-wiki-psp/src/master/book/1-ProgramacionMultiproceso/1-3-GestionDeProcesos.md) a los que hace referencia este ejemplo.

En este repo se tratan aspectos como:

* Creación de procesos
* Comandos para la gestión de procesos.
* Herramientas gráficas para la gestión de procesos

## Requisitos y dependencias

* Maven `v3.6.0` o superior
* Java `v1.7` o superior

## Objeto y alcance

Simplemente, prueba con el IDE que tú elijas el código de este proyecto. Familiarízate con maven y con el entorno de desarrollo y pruebas.

Piensa si puedes contribuir en algo (ver [Contribution guidelines](#Contribution-guidelines)). Échale imaginación y sugiere por medio de un pull request.

Utiliza el apartado issues para iniciar cualquier cuestión/sugerencia/tarea.

## Antes de ponerte a trabajar...

### Clona el repositorio

```
git clone <url>
```

## Contribution guidelines

* Escribir tests
* Revisión de código

## Who do I talk to?

* Propietario del repo, proferor


## Documentación adicional

* [Documento que describe cómo originalmente se planteó el ejemplo y cómo se generó la Aplicación `DocumentEditor.jar`](doc/Guia-Original-del-Ejemplo.md)
* [Java in Visual Studio Code](https://code.visualstudio.com/docs/languages/java)
* [Maven en 5 minutos](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)
* [Guía rápida de inicio con Maven](https://maven.apache.org/guides/getting-started/index.html#)