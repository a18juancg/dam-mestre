package psp.sampleCrearProcesos;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) {

		try {
			// Obtenemos el nombre del SO
			String osName = System.getProperty("os.name");
			if (osName.toUpperCase().contains("WIN")) { // Windows
				//TODO:
				
			} else { // Linux or MacOS
				Process nuevoProceso = Runtime.getRuntime().exec("ls");
				System.out.println("Directorio actual " + System.getProperty("user.dir"));
				nuevoProceso = Runtime.getRuntime().exec("java -jar lib/DocumentEditor.jar");
				System.out.println("Se ha creado un nuevo proceso del Editor.");
				OutputStream outputStream = nuevoProceso.getOutputStream();
				PrintStream printStream = new PrintStream(outputStream);
				printStream.println();
				printStream.flush();
				printStream.close();
				
				if(osName.toUpperCase().contains("MAC")){
					//Aquí se levantan dos procesos: open y la propia calculadora a instancia del proceso open
					Runtime.getRuntime().exec("open -na calculator");
					System.out.println("Se ha creado un nuevo proceso de la calculadora de mac");
				}
			}

		} catch (SecurityException ex) {
			System.out.println(
					"Ha ocurrido un error de Seguridad." + "No se ha podido crear el proceso por falta de permisos.");
		} catch (Exception ex) {
			System.out.println("Ha ocurrido un error, descripción: " + ex.toString());
		}

	}
}
